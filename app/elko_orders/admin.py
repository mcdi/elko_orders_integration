from django.contrib import admin
from .models import Shop, Posting, Transaction, Product

admin.site.register(Shop)

class PostingAdmin(admin.ModelAdmin):
    search_fields = ['posting_number']
    list_display = ('shop', 'posting_number', 'accepted', 'status')

admin.site.register(Posting, PostingAdmin)

class ProductAdmin(admin.ModelAdmin):
    search_fields = ['offer_id', 'barcode', 'posting__posting_number']
    list_display = ('posting_number','offer_id', 'barcode')

    def posting_number(self, obj):
        return obj.posting.posting_number

admin.site.register(Product, ProductAdmin)

class TransactionAdmin(admin.ModelAdmin):
    search_fields = ['posting__posting_number']
    list_display = ('posting_number', 'transaction_number', 'tran_date', 'total_amount')

    def posting_number(self, obj):
        return obj.posting.posting_number

admin.site.register(Transaction, TransactionAdmin)
