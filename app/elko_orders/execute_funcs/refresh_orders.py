from ..models import Shop, Posting, Product
import datetime
from ..lib.ozon_global_methods import collect_orders
from ..lib.ozon_requests import get_product_info
from typing import List
from django.utils import timezone
import json


def create_orders(shop:Shop, ozon_orders_data:List[dict]):
    """
    Function must create orders in DB and included products.
    :param ozon_orders_data: unmodified orders data list from OzonAPI
    :return:
    """
    for order in ozon_orders_data:
        new_order = Posting.ozon_data_connector(shop, order)
        new_order.save()
        for product in order["products"]:
            offer_id = product["offer_id"]
            product_info_response = get_product_info(shop.client_id, shop.api_key, offer_id)
            try:
                info_data = json.loads(product_info_response.text)["result"]
                barcode = info_data["barcode"]
            except:
                barcode = None
            new_product = Product(
                posting_id=new_order.id,
                offer_id=offer_id,
                name=product["name"],
                sku=product["sku"],
                quantity=product["quantity"],
                barcode=barcode
            )
            new_product.save()

def update_orders(shop:Shop, ozon_orders_data:List[dict]):
    """
    Function must update orders in DB and included products.
    :param ozon_orders_data: unmodified orders data list from OzonAPI
    :return:
    """
    for order in ozon_orders_data:
        Posting.objects.update_or_create(
            shop=shop,
            posting_number=order["posting_number"],
            defaults={
                "status": order["status"],
                "cancel_reason_id": int(order["cancel_reason_id"])
            }
        )


def refresh_orders(shop: Shop):
    date_from = shop.get_oldest_active_order_datetime()
    if not date_from:
        date_from = datetime.datetime.now() - datetime.timedelta(days=2)
    else:
        date_from = date_from.replace(tzinfo=None)
    orders = collect_orders(
        client_id=shop.client_id,
        api_key=shop.api_key,
        since=date_from,
        to=datetime.datetime.now(),
        statuses_list=["awaiting_deliver", "delivering", "delivered", "cancelled"]
    )

    exists_orders_in_database = Posting.objects.filter(created_at__gte=date_from).values_list("posting_number", flat=True)
    for_update = list(filter(lambda order:order["posting_number"] in exists_orders_in_database, orders))
    for_create = list(filter(lambda order:order["posting_number"] not in exists_orders_in_database, orders))

    create_orders(shop, for_create)
    update_orders(shop, for_update)

    shop.orders_refreshed_at = timezone.now()
    shop.save()
    return {
        "created": len(for_create),
        "updated": len(for_update)
    }
