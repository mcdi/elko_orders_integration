from ..models import Shop, Transaction, Posting
import datetime
from ..lib.ozon_global_methods import collect_all_transactions
from django.utils import timezone


def refresh_transactions(shop: Shop):
    date_from = shop.get_last_transtions_datetime()
    if not date_from:
        date_from = datetime.datetime.now() - datetime.timedelta(days=5)
    else:
        date_from = date_from.replace(tzinfo=None)

    transactions = collect_all_transactions(
        client_id=shop.client_id,
        api_key=shop.api_key,
        since=date_from,
        to=datetime.datetime.now()
    )

    transactions_created_count = 0
    transactions_updated_count = 0

    for transaction in transactions:
        postings = Posting.objects.filter(posting_number=transaction["orderNumber"])
        defaults = {
            "transaction_type": transaction["transactionType"],
            "order_amount": transaction["orderAmount"],
            "discount_amount": transaction["discountAmount"],
            "commission_amount": transaction["commissionAmount"],
            "total_amount": transaction["totalAmount"],
            "tran_date": transaction["tranDate"],
            "delivery_amount": transaction["itemDeliveryAmount"]

        }
        for posting in postings:
            tran, created = Transaction.objects.update_or_create(
                transaction_number=transaction["transactionNumber"],
                posting=posting,
                defaults=defaults
            )
            if created:
                transactions_created_count += 1
            else:
                transactions_updated_count += 1
    shop.transactions_refreshed_at = timezone.now()
    shop.save()
    return {
        "transactions_created_count": transactions_created_count,
        "transactions_updated_count": transactions_updated_count
    }
