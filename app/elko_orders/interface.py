from typing import List, Union
from functools import wraps
from .models import Posting, Shop, Transaction
from .execute_funcs.refresh_orders import refresh_orders
from .execute_funcs.refresh_transactions import refresh_transactions


def shop_initialized(func):
    """
    Wrapper for exception methods of uninitialized shop
    """
    @wraps(func)
    def wrapper(instance, *args, **kwargs):
        if not instance.shop_model:
            return "This method uses only with initialised shop"
        else:
            return func(instance, *args, **kwargs)
    return wrapper


class ElkoShopInterface:

    shop_model = None

    def __init__(self, shop_client_id = None):
        self.shop_client_id = shop_client_id
        self.__initialize_shop_model()

    def __initialize_shop_model(self):
        self.shop_model = Shop.objects.get(client_id=self.shop_client_id)

    def create_shop(self, client_id, api_key):
        new_shop, created = Shop.objects.get_or_create(client_id=client_id, api_key=api_key)
        self.shop_model = new_shop
        if created:
            return "Shop created"
        else:
            return "Shop with this parameters exists in database"

    @shop_initialized
    def get_orders(self, posting_numbers: Union[List[str], str] = None):
        if not posting_numbers:
            postings = Posting.objects.filter(shop_id=self.shop_model.id, accepted=False)
            return postings
        elif type(posting_numbers) is str:
            postings = Posting.objects.filter(shop_id=self.shop_model.id, posting_number=posting_numbers)
            return postings
        elif type(posting_numbers) is list:
            postings = Posting.objects.filter(shop_id=self.shop_model.id, posting_number__in=posting_numbers)
            return postings

    @shop_initialized
    def accept_orders(self, posting_numbers: List[str] = None):
        updated_count = Posting.objects.filter(posting_number__in=posting_numbers).update(accepted=True)
        return {
            "sended_orders_count": len(posting_numbers),
            "accepted_orders_count": updated_count
        }

    @shop_initialized
    def get_transactions(self, posting_numbers: Union[List[str], str]):
        if type(posting_numbers) is str:
            transactions = Transaction.objects.filter(posting__posting_number=posting_numbers)
            return transactions
        elif type(posting_numbers) is list:
            transactions = Transaction.objects.filter(posting__posting_number__in=posting_numbers)
            return transactions

    @shop_initialized
    def refresh_orders_in_database(self):
        result = refresh_orders(self.shop_model)
        return result


    @shop_initialized
    def refresh_transactions_in_database(self):
        result = refresh_transactions(self.shop_model)
        return result