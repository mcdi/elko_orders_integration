import datetime
from typing import List
from elko_orders.lib.ozon_requests import get_fbo_postings, get_transactions
import json
from multiprocessing.pool import ThreadPool


class OzonRequestError(Exception):
    pass


def orders_requests_cycle_wrapper(args):

    client_id = args[0]
    api_key = args[1]
    since = args[2]
    to = args[3]
    status = args[4]
    offset_step = args[5]

    postings = []
    offset = 0
    offset_step = offset_step
    while True:
        postings_response = get_fbo_postings(
            client_id, api_key, since, to, status, offset_step, offset
        )
        try:
            result = json.loads(postings_response.text)["result"]
        except:
            raise OzonRequestError(postings_response.text)
        postings += result
        offset += offset_step
        if len(result) == 0:
            break
    return postings


def collect_orders(
        client_id: str,
        api_key: str,
        since: datetime.datetime,
        to: datetime.datetime,
        statuses_list: List[str],
) -> List[dict]:
    """
    Method for collect orders with complicated parameters(With a lot requests)
    :param client_id: str - Take it in Ozon Private cabinet
    :param api_key: str - Take it in Ozon Private cabinet
    :param since: datetime.datetime - Orders filter from this date
    :param to: datetime.datetime - Orders filter to this date
    :param statuses_list: List[str] -
    :return: List[dict]
    """
    offset_step = 500
    arg_set = []
    for status in statuses_list:
        arg_set.append(
            (client_id, api_key, since, to, status, offset_step, )
        )

    all_postings = []
    pool = ThreadPool(processes=len(statuses_list))
    try:
        result = pool.map(orders_requests_cycle_wrapper, arg_set)
        for r in result:
            all_postings += r
    except Exception as e:
        pool.close()
        raise e
    pool.close()
    return all_postings


def collect_all_transactions(
    client_id: str,
    api_key: str,
    since: datetime.datetime,
    to: datetime.datetime
) -> List[dict]:
    all_transactions = []
    page = 1
    while True:
        response = get_transactions(
            client_id=client_id,
            api_key=api_key,
            since=since,
            to=to,
            page=page,
            page_size=10000
        )
        transactions = json.loads(response.text)["result"]
        all_transactions += transactions
        page += 1
        if len(transactions) == 0:
            break
    return all_transactions

if __name__ == '__main__':

    orders = collect_orders(
        "32364",
        "b10f0758-dc64-4b8e-9917-da56066fc496",
        datetime.datetime.now() - datetime.timedelta(days=5),
        datetime.datetime.now(),
        statuses_list=["awaiting_deliver", "delivering", "delivered", "cancelled"]
    )
    print(orders)

