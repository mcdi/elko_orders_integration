import requests
import datetime


def get_fbo_postings(client_id:str, api_key:str, since:datetime.datetime, to:datetime.datetime, status:str, limit:int, offset:int):
    url = "http://api-seller.ozon.ru/v2/posting/fbo/list"
    headers = {
        "Client-Id": client_id,
        "Api-Key": api_key,
        "Content-Type": "application/json"
    }
    payload = {
        "dir": "asc",
        "filter":
            {
                "since": since.isoformat(timespec='milliseconds') + "Z",
                "status": status,
                "to": to.isoformat(timespec='milliseconds') + "Z"
            },
        "limit": limit,
        "offset": offset,
        "with": {
            "analytics_data": True,
            "barcodes": True,
            "financial_data": True
        }
    }
    response =  requests.post(
        url=url,
        headers=headers,
        json=payload
    )
    return response


def get_transactions(client_id:str, api_key:str, since:datetime.datetime, to:datetime.datetime,
                     page:int, page_size:int, transaction_type:str = "all", posting_number:str = None):
    url = "http://api-seller.ozon.ru/v2/finance/transaction/list"
    headers = {
        "Client-Id": client_id,
        "Api-Key": api_key,
        "Content-Type": "application/json"
    }
    payload = {
        "filter": {
            "date": {
                "from": since.isoformat(timespec='milliseconds') + "Z",
                "to": to.isoformat(timespec='milliseconds') + "Z"
            },
            "transaction_type": transaction_type,
        },
        "page": page,
        "page_size": page_size,
    }
    if posting_number:
        payload["filter"]["posting_number"] = posting_number
    response = requests.post(
        url=url,
        headers=headers,
        json=payload
    )
    return response

def get_product_info(client_id:str, api_key:str, offer_id:str):

    url = "http://api-seller.ozon.ru/v2/product/info"
    headers = {
        "Client-Id": client_id,
        "Api-Key": api_key,
        "Content-Type": "application/json"
    }
    payload = {
        "offer_id": offer_id
    }

    response = requests.post(
        url=url,
        headers=headers,
        json=payload
    )
    return response