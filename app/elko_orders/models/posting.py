from django.db import models
from .shop import Shop


class Posting(models.Model):
    """

    """
    shop = models.ForeignKey(Shop, on_delete=models.CASCADE)

    ozon_id = models.CharField(max_length=1024)
    posting_number = models.CharField(max_length=1024)
    status = models.CharField(max_length=1024)
    status_date = models.DateTimeField(auto_now=True)
    cancel_reason_id = models.IntegerField(default=0)
    created_at = models.DateTimeField()

    accepted = models.BooleanField(default=False)

    @classmethod
    def ozon_data_connector(cls, shop: Shop, ozon_dict_posting: dict):
        return cls(
            shop_id=shop.id,
            ozon_id=ozon_dict_posting["order_id"],
            posting_number=ozon_dict_posting["posting_number"],
            status=ozon_dict_posting["status"],
            cancel_reason_id=ozon_dict_posting["cancel_reason_id"],
            created_at=ozon_dict_posting["in_process_at"]
        )

    @property
    def products(self):
        return self.product_set.all()

    def __str__(self):
        return f"{self.shop.client_id}/{self.posting_number}"
