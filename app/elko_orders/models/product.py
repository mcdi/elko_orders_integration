from django.db import models
from .posting import Posting



class Product(models.Model):
    """

    """
    posting = models.ForeignKey(Posting, on_delete=models.CASCADE)

    sku = models.CharField(max_length=1024, verbose_name="Product SKU")
    offer_id = models.CharField(max_length=1024, verbose_name="Product offer id")
    name = models.CharField(max_length=1024, verbose_name="Product name")
    quantity = models.IntegerField(default=0, verbose_name="Product quantity")
    price = models.FloatField(default=0, verbose_name="Product price")

    barcode = models.CharField(max_length=255, verbose_name="Product Barcode", null=True, blank=True)
    
    def __str__(self):
        return f"Offer_id:{self.offer_id}"