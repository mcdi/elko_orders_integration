from django.db import models


class Shop(models.Model):
    """

    """
    client_id = models.CharField(max_length=1024, verbose_name="Ozon shop client id")
    api_key = models.CharField(max_length=1024, verbose_name="Ozon shop api key")

    created_at = models.DateTimeField(auto_now_add=True)

    orders_refreshed_at = models.DateTimeField(null=True, blank=True)
    transactions_refreshed_at = models.DateTimeField(null=True, blank=True)

    def __str__(self):
        return self.client_id

    def get_oldest_active_order_datetime(self):
        """
        Function returns datetime of the oldest posting with status 'awaiting_deliver' or 'delivering' or 'driver_pickup'
        :return:
        """
        filtered_postings = self.posting_set.filter(status__in=[
            "awaiting_deliver", "delivering", "driver_pickup"
        ]).order_by("created_at")
        try:
            return filtered_postings[0].created_at
        except:
            return None

    def get_last_transtions_datetime(self):
        from .transaction import Transaction
        transaction = Transaction.objects.all().order_by("tran_date").last()
        if transaction:
            return transaction.tran_date
        else:
            return None