from django.db import models
from .posting import Posting


class Transaction(models.Model):
    """

    """
    posting = models.ForeignKey(Posting, on_delete=models.CASCADE)

    transaction_number = models.CharField(max_length=1024, null=True, blank=True)
    transaction_type = models.CharField(max_length=1024)
    order_amount = models.FloatField(default=0)
    discount_amount = models.FloatField(default=0)
    commission_amount = models.FloatField(default=0)
    total_amount = models.FloatField(default=0)
    tran_date = models.DateTimeField()
    delivery_amount = models.FloatField(default=0)

    @property
    def posting_number(self):
        return self.posting.posting_number

    def __str__(self):
        return self.posting.posting_number