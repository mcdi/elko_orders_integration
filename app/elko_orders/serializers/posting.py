from rest_framework import serializers
from ..models import Posting
from .product import ProductSerializer

class PostingSerializer(serializers.ModelSerializer):
    products = ProductSerializer(many=True)

    class Meta:
        model = Posting
        fields = ['ozon_id', 'posting_number', 'status', 'status_date', 'cancel_reason_id', 'created_at', 'accepted', 'products']