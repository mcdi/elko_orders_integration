from rest_framework import serializers


class BoolResultSerializer(serializers.Serializer):

    result = serializers.BooleanField(default=True)



class CharResultSerializer(serializers.Serializer):

    result = serializers.CharField()