from celery import shared_task
from .models import Shop
from .interface import ElkoShopInterface


@shared_task
def refresh_orders_in_one_shop(shop_client_id:str) -> None:
    interface = ElkoShopInterface(shop_client_id)
    result = interface.refresh_orders_in_database()
    return result

@shared_task
def refresh_orders_in_all_shops() -> None:
    shops = Shop.objects.all()
    for shop in shops:
        refresh_orders_in_one_shop.delay(shop.client_id)


@shared_task
def refresh_transactions_in_one_shop(shop_client_id:str) -> None:
    interface = ElkoShopInterface(shop_client_id)
    result = interface.refresh_transactions_in_database()
    return result

@shared_task
def refresh_transactions_in_all_shops() -> None:
    shops = Shop.objects.all()
    for shop in shops:
        refresh_transactions_in_one_shop.delay(shop.client_id)