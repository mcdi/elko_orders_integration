from rest_framework.routers import SimpleRouter
from .views import PostingViewSet, TransactionViewSet




router = SimpleRouter()

router.register("postings", PostingViewSet, basename="postings")
router.register("transactions", TransactionViewSet, basename="transactions")

urlpatterns = router.urls
