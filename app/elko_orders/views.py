from drf_yasg import openapi
from drf_yasg.utils import swagger_auto_schema

from rest_framework.viewsets import ViewSet
from rest_framework.decorators import action
from rest_framework.response import Response

from .models import Posting, Transaction
from .serializers import PostingSerializer, TransactionSerializer, BoolResultSerializer, CharResultSerializer
from rest_framework.permissions import  IsAuthenticated, AllowAny

from .interface import ElkoShopInterface

class PostingViewSet(ViewSet):
    queryset = Posting.objects.all()
    serializer_class = PostingSerializer
    permission_classes = [IsAuthenticated]

    @swagger_auto_schema(
    manual_parameters=[
        openapi.Parameter('Client-Id', openapi.IN_HEADER, description="Shop Client id", type=openapi.TYPE_INTEGER, required=True),
    ],
    request_body=openapi.Schema(
        type=openapi.TYPE_OBJECT,
        required=[],
        properties={
            "posting_numbers": openapi.Schema(type="array", items=openapi.Schema(type="string"), description="Array unrequired or can be empty for all unaccepted transactions"),
            "offset": openapi.Schema(type="integer", description="Unrequired")
        }
    ),
    responses={
        200: openapi.Response('response description', PostingSerializer(many=True)),
        500: openapi.Response('Result:', CharResultSerializer)
    },
    tags=['Postings'])
    @action(methods=["post"], detail=False)
    def get(self, request):
        """
        <p>Method returns all unaccepted postings if posting_numbers field is empty.</p>
        Or returns list of posting objects for posting_numbers list.
        User offset param in query for next page orders.
        Max orders count for 1 response - 500.
        """
        try:
            client_id = int(request.headers["client-id"])
        except:
            client_id = None
        posting_numbers = request.data.get("posting_numbers", None)
        if posting_numbers and len(posting_numbers) == 0:
            posting_numbers = None
        offset = request.data.get("offset", 0)

        if client_id:
            interface = ElkoShopInterface(client_id)
            postings = interface.get_orders(posting_numbers)
            postings = postings[offset:offset+500]
            serializer = PostingSerializer(postings, many=True)
            return Response(serializer.data)
        else:
            serializer = CharResultSerializer(data={"result": "Client-Id is required in header."})
            serializer.is_valid()
            return Response(serializer.data, status=500)

    @swagger_auto_schema(
    manual_parameters=[
        openapi.Parameter('Client-Id', openapi.IN_HEADER, description="Shop Client id", type=openapi.TYPE_INTEGER, required=True),
    ],
    request_body=openapi.Schema(
        type=openapi.TYPE_OBJECT,
        properties={
            "posting_numbers": openapi.Schema(type="array", items=openapi.Schema(type="string"))
        }
    ),
    responses={
        200: openapi.Response('Result:', BoolResultSerializer),
        500: openapi.Response('Result:', CharResultSerializer)
    },
    tags=['Postings'])
    @action(methods=["POST"], detail=False)
    def accept(self, request):
        """
        Method accepts all unaccepted postings from posting_numbers list.
        """
        try:
            client_id = request.headers["client-id"]
        except:
            client_id = None
        try:
            try:
                posting_numbers = request.data["posting_numbers"]
            except:
                posting_numbers = request.POST["posting_numbers"]
        except:
            posting_numbers = None
        if client_id:
            interface = ElkoShopInterface(client_id)
            try:
                interface.accept_orders(posting_numbers)
                serializer = BoolResultSerializer({"result": True})
                if serializer.is_valid():
                    return Response(serializer.data)
            except:
                serializer = BoolResultSerializer({"result": False})
                if serializer.is_valid():
                    return Response(serializer.data)
        else:
            serializer = CharResultSerializer(data={"result": "Client-Id is required in header."})
            serializer.is_valid()
            return Response(serializer.data, status=500)

class TransactionViewSet(ViewSet):

    queryset = Transaction.objects.all()
    serializer_class = TransactionSerializer
    permission_classes = [IsAuthenticated]

    @swagger_auto_schema(
    manual_parameters=[
        openapi.Parameter('Client-Id', openapi.IN_HEADER, description="Shop Client id", type=openapi.TYPE_INTEGER, required=True),
    ],
    request_body=openapi.Schema(
        type=openapi.TYPE_OBJECT,
        properties={
            "posting_numbers": openapi.Schema(type="array", items=openapi.Schema(type="string"), description="Array can be empty"),
            "offset": openapi.Schema(type="integer")
        }
    ),
    responses={
        200: openapi.Response('response description', TransactionSerializer(many=True)),
        500: openapi.Response('Result:', CharResultSerializer)
    },
    tags=['Transactions'])
    @action(methods=["post"], detail=False)
    def get(self, request):
        try:
            client_id = request.headers["client-id"]
        except:
            client_id = None
        posting_numbers = request.data.get("posting_numbers", None)
        if posting_numbers and len(posting_numbers) == 0:
            posting_numbers = None
        offset = request.data.get("offset", 0)
        if client_id:
            interface = ElkoShopInterface(client_id)
            transactions = interface.get_transactions(posting_numbers)
            transactions = transactions[offset: offset + 500]
            serializer = TransactionSerializer(transactions, many=True)
            return Response(serializer.data)
        else:
            serializer = CharResultSerializer(data={"result": "Client-Id is required in header."})
            serializer.is_valid()
            return Response(serializer.data, status=500)