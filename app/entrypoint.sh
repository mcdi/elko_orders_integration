#!/bin/bash

#echo "Waiting for postgres..."
#
#    while ! nc -z $POSTGRES_HOST ; do
#      sleep 0.1
#    done
#
#echo "PostgreSQL started"


cd /code/app
python manage.py makemigrations
python manage.py migrate
echo "from docker_run import init_superuser; init_superuser();" | python manage.py shell
python manage.py collectstatic --no-input
gunicorn core.wsgi:application --bind 0.0.0.0:8000 --reload